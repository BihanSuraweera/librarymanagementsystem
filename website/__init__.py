from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from os import path
from flask_login import LoginManager

db = SQLAlchemy()
DB_NAME = "database.db"


def create_app():
    app = Flask(__name__)
    app.config['SECRET_KEY'] = '123456789'
    app.config['SQLALCHEMY_DATABASE_URI'] = f'sqlite:///{DB_NAME}'
    db.init_app(app)

    from .api import api

    app.register_blueprint(api, url_prefix='/')

    from .models import Admin, User, Book, Burrow

    create_database(app)

    login_manager = LoginManager()
    login_manager.login_view = 'api.login'
    login_manager.init_app(app)

    @login_manager.user_loader
    def load_admin(id):
        return Admin.query.get(int(id))

    return app


def create_database(app):
    if not path.exists('website/' + DB_NAME):
        db.create_all(app=app)
        print('Created Database')
