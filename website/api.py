from flask import Blueprint, render_template, request, flash, redirect, url_for
from sqlalchemy.sql.functions import user
from .models import Admin, User, Book, Burrow
from werkzeug.security import generate_password_hash, check_password_hash
from . import db
from flask_login import login_user, login_required, logout_user, current_user
from datetime import datetime
import sqlite3 as sql

api = Blueprint('api', __name__)

# -----------------Home-----------------------------------------------------------------------------------------------------------------------------------


@api.route('/')
@login_required
def home():
    return render_template("home.html", user=current_user)


# -----------------Login-----------------------------------------------------------------------------------------------------------------------------------


@api.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        email = request.form.get('email')
        password = request.form.get('password')

        admin = Admin.query.filter_by(email=email).first()
        if admin:
            if check_password_hash(admin.password, password):
                flash('Logged in successfully!', category='success')
                login_user(admin, remember=True)
                return redirect(url_for('api.home'))
            else:
                flash('Incorrent password, try again', category='error')
        else:
            flash('Email does not exist', category='error')

    return render_template("login.html", user=current_user)

# -----------------Logout----------------------------------------------------------------------------------------------------------------------------------


@api.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('api.login'))


# -----------------Register-----------------------------------------------------------------------------------------------------------------------------------

@api.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == 'POST':
        userFirstName = request.form.get('userFirstName')
        userLastName = request.form.get('userLastName')
        address = request.form.get('address')
        phoneNumber = request.form.get('phoneNumber')

        if len(userFirstName) < 2:
            flash('First name must be grater than 2 characters', category='error')
        elif len(userLastName) < 2:
            flash('Last name must be grater than 2 characters', category='error')
        elif len(address) < 10:
            flash('address must be grater than 10 characters', category='error')
        elif len(phoneNumber) != 10:
            flash('Phone number must be 10 characters long', category='error')
        else:
            new_user = User(first_name=userFirstName, last_name=userLastName,
                            address=address, phone_number=phoneNumber)
            db.session.add(new_user)
            db.session.commit()
            flash('Library user created', category='success')
            return redirect(url_for('api.home'))

    return render_template("register.html", user=current_user)


# -----------------Sign Up-----------------------------------------------------------------------------------------------------------------------------------

@api.route('/sign-up', methods=['GET', 'POST'])
def sign_up():
    if request.method == 'POST':
        email = request.form.get('email')
        adminName = request.form.get('adminName')
        password1 = request.form.get('password1')
        password2 = request.form.get('password2')

        admin = Admin.query.filter_by(email=email).first()
        if admin:
            flash('Email already exists', category='error')
        elif len(email) < 4:
            flash('Email must be grater than 4 characters', category='error')
        elif len(adminName) < 2:
            flash('Name must be grater than 2 characters', category='error')
        elif password1 != password2:
            flash('Password do not match', category='error')
        elif len(password1) < 7:
            flash('Password must be grater than 7 characters', category='error')
        else:
            new_admin = Admin(email=email, adminName=adminName,
                              password=generate_password_hash(password1, method='sha256'))
            db.session.add(new_admin)
            db.session.commit()
            flash('Librarian account created', category='success')
            return redirect(url_for('api.login'))

    return render_template("sign_up.html", user=current_user)


# -----------------Burrow Books-----------------------------------------------------------------------------------------------------------------------------------


@api.route('/burrow-books', methods=['GET', 'POST'])
@login_required
def burrow_books():
    if request.method == 'POST':
        userId = request.form.get('userId')
        book1 = request.form.get('book1')
        book2 = request.form.get('book2')

        user_id_number = User.query.filter_by(id=userId).first()
        bookId1 = Book.query.filter_by(id=book1).first()
        bookId2 = Book.query.filter_by(id=book2).first()
        times = Burrow.query.filter_by(user_id=userId).count()

        if times >= 2:
            flash('Already Burrowed 2 books form library', category='error')
        elif userId == '':
            flash('Please enter a Library user\'s ID', category='error')
        elif not user_id_number:
            flash('Library user not found', category='error')
        elif book1 == '':
            flash('Please Enter the First Book', category='error')
        elif not bookId1:
            flash('First Book Does not Exist', category='error')
        else:
            burrow = Burrow(user_id=userId, book_id=book1)
            db.session.add(burrow)
            db.session.commit()

            bookId1.stock = bookId1.stock - 1
            db.session.commit()

            if not bookId2 and book2 != '':
                flash('First Book added, but Second book Does not Exist',
                      category='error')
            elif bookId2:
                burrow = Burrow(user_id=userId, book_id=book2)
                db.session.add(burrow)
                db.session.commit()

                bookId2.stock = bookId2.stock - 1
                db.session.commit()

            flash('Books Burrowed', category='success')

    return render_template("burrow-books.html", user=current_user)


# -----------------Return Books-----------------------------------------------------------------------------------------------------------------------------------


@api.route('/return-books', methods=['GET', 'POST'])
@login_required
def return_books():
    if request.method == 'POST':
        userId = request.form.get('userId')
        book1 = request.form.get('book1')
        book2 = request.form.get('book2')

        user_id_number = User.query.filter_by(id=userId).first()
        bookId1 = Book.query.filter_by(id=book1).first()
        bookId2 = Book.query.filter_by(id=book2).first()
        times = Burrow.query.filter_by(user_id=userId).count()
        burrow_days = 7

        if userId == '':
            flash('Please enter a Library user\'s ID', category='error')
        elif not user_id_number:
            flash('Library user not found', category='error')
        elif book1 == '':
            flash('Please Enter the First Book', category='error')
        elif not bookId1:
            flash('First Book Does not Exist', category='error')
        else:
            burrowed_time = Burrow.query.filter_by(
                user_id=userId, book_id=book1).first()
            time_deferance = datetime.now() - burrowed_time.burrow_date
            fine = time_deferance.days * 10
            if time_deferance.days > burrow_days:
                flash('First book was returned late. Fine = LKR' +
                      fine, category='error')
            Burrow.query.filter_by(user_id=userId, book_id=book1).delete()
            db.session.commit()

            bookId1.stock = bookId1.stock + 1
            db.session.commit()

            if not bookId2 and book2 != '':
                flash('First Book added, but Second book Does not Exist',
                      category='error')
            elif bookId2:
                burrowed_time = Burrow.query.filter_by(
                    user_id=userId, book_id=book2).first()
                time_deferance = datetime.now() - burrowed_time.burrow_date
                fine = time_deferance.days * 10
                if time_deferance.days > burrow_days:
                    flash('Second book was returned late. Fine = LKR' +
                          fine, category='error')
                Burrow.query.filter_by(user_id=userId, book_id=book2).delete()
                db.session.commit()

                bookId2.stock = bookId2.stock + 1
                db.session.commit()

            flash('Books Returned', category='success')

    return render_template("return-books.html", user=current_user)


# -----------------Add Books-----------------------------------------------------------------------------------------------------------------------------------


@ api.route('/add-books', methods=['GET', 'POST'])
@ login_required
def add():
    if request.method == 'POST':
        bookTitle = request.form.get('bookName')
        bookAuthor = request.form.get('bookAuthor')
        bookCopies = request.form.get('bookCopies')

        book = Book.query.filter_by(title=bookTitle).first()

        if bookCopies == '':
            flash('Enter Number of Copies', category='error')
        elif book:
            book.stock = book.stock + int(bookCopies)
            db.session.commit()
            flash('Book Added', category='success')
        else:
            add_book = Book(title=bookTitle, author=bookAuthor,
                            stock=int(bookCopies))
            db.session.add(add_book)
            db.session.commit()
            flash('Book Added', category='success')

    return render_template("add_books.html", user=current_user)
    # return {"success": "true"}

# -----------------Display Books-----------------------------------------------------------------------------------------------------------------------------------


@ api.route('/books', methods=['GET'])
@ login_required
def display_books():
    con = sql.connect(
        'C:/Users/PCN/Desktop/Library_management/website/database.db')
    cur = con.cursor()
    cur.execute('SELECT * FROM book')
    output_data = cur.fetchall()
    # print(output_data)
    return render_template('books.html', output_data=output_data, user=current_user)
